
var express = require('express');
var router = express.Router(); 
var amqp = require('amqplib/callback_api');
var amqpConn = null;
var pubChannel = null;
var offlinePubQueue = [];


//----------------------------------------------------------------------------//


function start() {
    try {
        amqp.connect('amqps://sgisntca:sytsfABPmbtLZcvLLHvcprkMlygkGvbw@chimpanzee.rmq.cloudamqp.com/sgisntca', function (err, conn) {
            if (err) {
                console.error("[AMQP]", err.message);
                return setTimeout(start, 1000);
            }
            conn.on("error", function (err) {
                if (err.message !== "Connection closing") {
                    console.error("[AMQP] conn error", err.message);
                }
            });
            conn.on("close", function () {
                console.error("[AMQP] reconnecting");
                return setTimeout(start, 1000);
            });
            console.log("[AMQP] connected");
            amqpConn = conn;
            whenConnected();
        });
        return true;
    }catch (e){
        return false;
    }

}

function whenConnected() {
    try{
        startPublisher();
        startWorker();
        return true;
    }catch (e){
        return false;
    }

}


function startPublisher() {
    amqpConn.createConfirmChannel(function (err, ch) {
        if (closeOnErr(err))
            return;
        ch.on("error", function (err) {
            console.error("[AMQP] channel error", err.message);
        });
        ch.on("close", function () {
            console.log("[AMQP] channel closed");
        });

        pubChannel = ch;
        while (true) {
            var m = offlinePubQueue.shift();
            if (!m)
                break;
            publish(m[0], m[1], m[2]);
        }
    });
}

function publish(exchange, routingKey, content) {
    try {
        pubChannel.publish(exchange, routingKey, content, {persistent: true},
                function (err, ok) {
                    if (err) {
                        console.error("[AMQP] publish1", err);
                        offlinePubQueue.push([exchange, routingKey, content]);
                        pubChannel.connection.close();
                    }
                });
    } catch (e) {
        console.error("[AMQP] publish2", e.message);
        offlinePubQueue.push([exchange, routingKey, content]);
    }
}
// A worker that acks messages only if processed succesfully
function startWorker() {
    amqpConn.createChannel(function (err, ch) {
        if (closeOnErr(err))
            return;
        ch.on("error", function (err) {
            console.error("[AMQP] channel error", err.message);
        });

        ch.on("close", function () {
            console.log("[AMQP] channel closed");
        });

        ch.prefetch(10);
        ch.assertQueue("sensores", {durable: true}, function (err, _ok) {
            if (closeOnErr(err))
                return;
            //ch.consume("jobs", processMsg, {noAck: false});
            console.log("Worker is started");
        });

        function processMsg(msg) {
            work(msg, function (ok) {
                try {
                    if (ok)
                        ch.ack(msg);
                    else
                        ch.reject(msg, true);
                } catch (e) {
                    closeOnErr(e);
                }
            });
        }
    });
}

function work(msg, cb) {
    data = msg.content.toString();
    console.log("Got msg ", msg.content.toString());
    console.log(data);
    cb(true);
}

function closeOnErr(err) {
    if (!err)
        return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}

function salida(codigo, entrada) {
    var today = new Date();
    var date = today.getFullYear() + '-' + today.getMonth() + '-' + today.getDay();

    if (codigo == '200') return {
        mensaje : "Operación correcta",
        fecha : date,
        resultado : entrada
    }

    if (codigo=='500') return {
        mensaje : "Ocurrió un error",
        fecha : date,
        resultado : entrada
    }
}

start();


/* GET home page. */
router.get('/', function (req, res, next) {
    try{
        res.render('home', {title: 'Home'});
        //res.status(200).json(salida("200", "[AMQP] publish Ok"));
    }catch (error) {
        res.status(500).json(salida("500",error));
    }
});

/* POST home page. */
router.post('/', function (req, res, next) {
});

router.post('/publish', function (req, res, next) {
    try {
        publish("", "sensores", new Buffer.from(JSON.stringify(req.body)));
        res.status(200).json(salida("200", "[AMQP] publish Ok"));
    } catch (error) {
        return res.status(500).json(salida("500", error));
    }
});

module.exports = router;


