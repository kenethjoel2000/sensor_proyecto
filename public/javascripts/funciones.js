let con = document.getElementById('tbodycon');
let min = document.getElementById('minutos');
let seg = document.getElementById('segundos');

var cronometro;




function  generardatos() {
    datos.forEach(function (valor, indice, array) {
        valor.trafico = Math.floor(Math.random() * (11 - 1)) + 1;
    });
};



function  llenartabla() {
    con.innerHTML = '';
    datos.forEach(function (valor, indice, array) {
        con.innerHTML = con.innerHTML +
                '<tr>' +
                '<td>' + valor.calle + '</td>' +
                '<td>' + valor.latitud + '</td>' +
                '<td>' + valor.longitud + '</td>' +
                '<td>' + valor.trafico + '</td>' +
                '</tr>';       
    });
};

function onload() {
    generardatos();
    llenartabla();
    envio();
    time();
    timeout();
};


function envio() {
    var xhtml = new XMLHttpRequest();
    xhtml.open('POST', '/publish', true);
    xhtml.setRequestHeader("Content-Type", "application/json");
    xhtml.send(JSON.stringify(datos));
};


function time() {
    var s = 0;
    var m = 0;
    cronometro = setInterval(function () {
        if (s === 60) {
            s = 0;
            m++;
            min.innerHTML = m;
            if (m === 0) {
                m = 0;
            }
        }
        seg.innerHTML = s;
        s++;
    }, 1000);
}

function timeout() {
    setTimeout(function () {
        console.log("Mensaje");
        clearInterval(cronometro);
        onload();
    }, timereset);
}

//let BotonGenerar=document.getElementById('buttonsend');
//
//BotonGenerar.onclick = function(){
//    let xhtml=new XMLHttpRequest();
//    xhtml.open('GET','datos',true);
//    xhtml.send();
//    xhtml.onreadystatechange= function(){
//        if(this.readyState==4 && this.status==200){
//            let data = this.responseText;
//            data = JSON.parse(data);
//            
//        }
//   }
//}

